# Brad's Powertools

## What is this?

**Powertools** a Docker image with a few convenient tools preinstalled. Upon startup, it always generates new keys for SSH, and it starts [Dropbear SSH](https://matt.ucc.asn.au/dropbear/dropbear.html) and [Jupyter Notebook](https://jupyter-notebook.readthedocs.io/en/stable/) listening on ports `22122` and `8888` respectively.

Note: The SSH server is Dropbear instead of OpenSSH because OpenSSH is a pain to get working when not running as the root user.


## Basic Use

Starting the container:

```
docker run -it -p 22122:22122 -p 8888:8888 registry.gitlab.com/infinitewarp/powertools:latest
```

Upon startup, observe the console, and copy the generated private key:

```
-----BEGIN OPENSSH PRIVATE KEY-----
[...]
-----END OPENSSH PRIVATE KEY-----
```

Save it to your local system and `chmod 600` it.

```
pbpaste > /tmp/id_ecdsa && \
    echo >> /tmp/id_ecdsa && \
    chmod 600 /tmp/id_ecdsa
```

If deployed to OpenShift, remember to port-forward before attempting to connect.

```
oc port-forward $(oc get pods -o jsonpath='{.items[?(.status.phase=="Running")].metadata.name}' | grep -e '^powertools-' | head -n1 ) 22122
```

Now, try to connect using your saved key over the forwarded port.

```
ssh \
    -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no \
    -p 22122 -i /tmp/id_ecdsa \
    powertools@localhost
```

Or if you want to scp files, the args are slightly different:
```
scp \
    -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no \
    -P 22122 -i /tmp/id_ecdsa \
    powertools@localhost:/tmp/fulldump.txt /tmp/
```


## Building

Build and push an image to registry. This will be different if you are not me. :)

```
docker login registry.gitlab.com
NAME=registry.gitlab.com/infinitewarp/powertools
docker build -t $NAME:latest .
docker run -p22122:22122 $NAME:latest
docker push $NAME:latest
```

Force OpenShift image streams to fetch new image. This only makes sense if you already have a project named "powertools" deployed.

```
oc login https://api.insights-dev.openshift.com --token=secret
oc import-image powertools
```
