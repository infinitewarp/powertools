FROM fedora:31

RUN dnf update -y && \
    dnf install -y \
      git which nss_wrapper ed tree procps-ng \
      dropbear iputils iproute bind-utils nmap-ncat rsync \
      postgresql python3-devel python3-pip && \
    dnf clean all && \
    rm -rf /var/cache/dnf

# Create powertools user.
RUN adduser --system -s /bin/bash -u 74205 -g 0 -d /home/powertools -M powertools
RUN echo 'powertools:powertools' | chpasswd

# Broaden permissions to deal with OpenShift uid problems.
RUN chmod 775 /home && \
    chmod 664 /etc/passwd /etc/group

RUN mkdir -p /etc/dropbear && \
    chmod 775 /etc/dropbear

# dropbear ssh (22122)
# jupyter notebook (8888)
EXPOSE 22122 8888

RUN /usr/bin/python3 -m pip install --upgrade \
    jupyter httpie requests faker psycopg2-binary

USER powertools

# This /etc/passwd mess deals with the fact that OpenShift refuses
# to use the requested user, and we may have to smash the current
# user's uid into where the "powertools" user is defined.
# I can't use `sed -i` here because it fails to write its temp file.
# I could have it write to a /tmp file and then copy if over,
# but plain `ed` can do this as a one-liner.
CMD echo -e ",s/74205/`id -u`/g\\012 w" | ed -s /etc/passwd && \
    dropbearkey -t rsa -f /etc/dropbear/dropbear_rsa_host_key && \
    dropbearkey -t dss -f /etc/dropbear/dropbear_dss_host_key && \
    dropbearkey -t ecdsa -f /etc/dropbear/dropbear_ecdsa_host_key && \
    mkdir -p /home/powertools/.ssh && \
    chmod 700 /home/powertools/.ssh && \
    touch /home/powertools/.ssh/authorized_keys && \
    chmod 600 /home/powertools/.ssh/authorized_keys && \
    ssh-keygen -N "" -t ecdsa -b 521 -f /home/powertools/.ssh/id_ecdsa && \
    cat /home/powertools/.ssh/id_ecdsa.pub >> /home/powertools/.ssh/authorized_keys && \
    cat /home/powertools/.ssh/id_ecdsa && \
    mkdir -p /home/powertools/jupyter && \
    HOME=/home/powertools /usr/local/bin/jupyter-notebook --notebook-dir=/home/powertools/jupyter --ip=0.0.0.0 --port=8888 &> /tmp/jupyter.out & \
    dropbear -E -F -R -w -g -a -p 22122 -P /home/powertools/dropbear.pid &> /tmp/dropbear.out & \
    tail -f --retry /tmp/jupyter.out /tmp/dropbear.out
